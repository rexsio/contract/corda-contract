package io.rexsio.corda.notarization.state

import io.rexsio.corda.notarization.contract.NotarizationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party

@BelongsToContract(NotarizationContract::class)
data class NotarizationState(val hash: String,
                             val provider: Party,
                             val receiver: Party) : ContractState {
    override val participants get() = listOf(provider, receiver)
}