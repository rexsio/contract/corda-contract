package io.rexsio.corda.notarization.workflow

import co.paralleluniverse.fibers.Suspendable
import io.rexsio.corda.notarization.contract.NotarizationCommands
import io.rexsio.corda.notarization.contract.NotarizationContract
import io.rexsio.corda.notarization.state.NotarizationState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.flows.CollectSignaturesFlow.Companion.tracker
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker

@InitiatingFlow
@StartableByRPC
class NotarizationFlow(private val hash: String,
                       private val otherParty: Party) : FlowLogic<List<SignedTransaction>>() {

    override val progressTracker = ProgressTracker()

    @Suspendable
    override fun call(): List<SignedTransaction> {
        val notary = serviceHub.networkMapCache.notaryIdentities[0]

        // Init
        val outputState = NotarizationState(hash, ourIdentity, otherParty)
        val initialTx = createInitialTransaction(notary, outputState)
        val otherPartySession = initiateFlow(otherParty)
        val fullySignedInitialTx = subFlow(CollectSignaturesFlow(initialTx, listOf(otherPartySession), tracker()))
        val finishedInitTx = subFlow(FinalityFlow(fullySignedInitialTx, otherPartySession))

        // Notarize
        val stateRef = StateRef(finishedInitTx.id, 0)
        val oldStateRef = serviceHub.toStateAndRef<NotarizationState>(stateRef)
        val notarizeTx = createNotarizeTransaction(notary, oldStateRef)
        val fullySignedNotarizeTx = subFlow(CollectSignaturesFlow(notarizeTx, listOf(otherPartySession), tracker()))
        val finishedNotarizeTx = subFlow(FinalityFlow(fullySignedNotarizeTx, otherPartySession))

        return listOf(finishedInitTx, finishedNotarizeTx)
    }

    private fun createInitialTransaction(notary: Party, outputState: NotarizationState): SignedTransaction {
        val signers = listOf(ourIdentity.owningKey, otherParty.owningKey)

        val txBuilder = TransactionBuilder(notary = notary)
                .addOutputState(outputState, NotarizationContract.ID)
                .addCommand(Command(NotarizationCommands.Init(), signers))

        txBuilder.verify(serviceHub)
        return serviceHub.signInitialTransaction(txBuilder)
    }

    private fun createNotarizeTransaction(notary: Party, oldStateRef: StateAndRef<NotarizationState>): SignedTransaction {
        val signers = listOf(ourIdentity.owningKey, otherParty.owningKey)

        val txBuilder: TransactionBuilder = TransactionBuilder(notary)
                .addInputState(oldStateRef)
                .addCommand(Command(NotarizationCommands.Notarize(), signers))

        txBuilder.verify(serviceHub)

        return serviceHub.signInitialTransaction(txBuilder)
    }

}

@InitiatedBy(NotarizationFlow::class)
class NotarizationFlowResponder(private val otherPartySession: FlowSession) : FlowLogic<List<SignedTransaction>>() {

    @Suspendable
    override fun call(): List<SignedTransaction> {
        val signedInitTx = signInitTransaction()
        val signedNotarizeTx = signNotarizeTransaction()
        return listOf(signedInitTx, signedNotarizeTx)
    }

    @Suspendable
    private fun signInitTransaction(): SignedTransaction {
        val flow = object : SignTransactionFlow(otherPartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                val output = stx.tx.outputs.single().data
                "This must be an NotarizationState transaction." using (output is NotarizationState)
            }
        }
        val expectedTxId = subFlow(flow).id
        return subFlow(ReceiveFinalityFlow(otherPartySession, expectedTxId))
    }

    @Suspendable
    private fun signNotarizeTransaction(): SignedTransaction {
        val flow = object : SignTransactionFlow(otherPartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                "There should be one input." using (stx.tx.inputs.size == 1)
            }
        }
        val expectedTxId = subFlow(flow).id
        return subFlow(ReceiveFinalityFlow(otherPartySession, expectedTxId))
    }

}
