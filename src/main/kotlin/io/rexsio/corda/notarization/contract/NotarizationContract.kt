package io.rexsio.corda.notarization.contract

import io.rexsio.corda.notarization.state.NotarizationState
import net.corda.core.contracts.*
import net.corda.core.contracts.Requirements.using
import net.corda.core.transactions.LedgerTransaction

class NotarizationContract : Contract {
    companion object {
        const val ID: ContractClassName = "io.rexsio.corda.notarization.contract.NotarizationContract"
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<NotarizationCommands>()

        when (command.value) {
            is NotarizationCommands.Init -> {
                requireThat {
                    "There should be no input state of type NotarizationState." using (tx.inputs.isEmpty())
                    "There should be one output state of type NotarizationState." using (tx.outputs.size == 1)

                    val output = tx.outputsOfType<NotarizationState>().single()
                    verifyState(output, command)
                }
            }

            is NotarizationCommands.Notarize -> {
                requireThat {
                    "There should be one input state of type NotarizationState." using (tx.inputs.size == 1)
                    "There should be no output state of type NotarizationState." using (tx.outputs.isEmpty())

                    val input = tx.inputsOfType<NotarizationState>().single()
                    verifyState(input, command)
                }
            }

        }
    }

    private fun verifyState(state: NotarizationState, command: CommandWithParties<NotarizationCommands>) {
        "The hash's value must be non-empty." using (state.hash.isNotEmpty())
        "The provider and the receiver cannot be the same entity." using (state.provider != state.receiver)

        val expectedSigners = listOf(state.provider.owningKey, state.receiver.owningKey)
        "There must be two signers." using (command.signers.toSet().size == 2)
        "The provider and receiver must be signers." using (command.signers.containsAll(expectedSigners))
    }

}
