package io.rexsio.corda.notarization.contract

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.TypeOnlyCommandData

interface NotarizationCommands : CommandData {
    class Init : TypeOnlyCommandData(), NotarizationCommands
    class Notarize : TypeOnlyCommandData(), NotarizationCommands
}
