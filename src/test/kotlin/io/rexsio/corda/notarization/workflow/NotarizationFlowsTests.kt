package io.rexsio.corda.notarization.workflow

import io.rexsio.corda.notarization.state.NotarizationState
import net.corda.core.contracts.TransactionVerificationException
import net.corda.core.utilities.getOrThrow
import net.corda.testing.core.singleIdentity
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.TestCordapp
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class NotarizationFlowsTests {
    private val network = MockNetwork(MockNetworkParameters(cordappsForAllNodes = listOf(
            TestCordapp.findCordapp("io.rexsio.corda.notarization")
    )))
    private val nodeA = network.createNode()
    private val nodeB = network.createNode()

    init {
        listOf(nodeA, nodeB).forEach {
            it.registerInitiatedFlow(NotarizationFlowResponder::class.java)
        }
    }

    @Before
    fun setup() = network.runNetwork()

    @After
    fun tearDown() = network.stopNodes()

    @Test
    fun `flow records a transactions in both parties transaction storages`() {
        val flow = NotarizationFlow(
                "f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86",
                nodeB.info.singleIdentity()
        )
        val future = nodeA.startFlow(flow)
        network.runNetwork()

        future.getOrThrow().forEach {
            for (node in listOf(nodeA, nodeB)) {
                assertEquals(it, node.services.validatedTransactions.getTransaction(it.id))
            }
        }
    }

    @Test
    fun `flow rejects invalid hash`() {
        val flow = NotarizationFlow("", nodeB.info.singleIdentity())

        val future = nodeA.startFlow(flow)
        network.runNetwork()

        assertFailsWith<TransactionVerificationException> { future.getOrThrow() }
    }

    @Test
    fun `SignedTransactions returned by the flow are signed by two sides`() {
        val flow = NotarizationFlow(
                "f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86",
                nodeB.info.singleIdentity()
        )
        val future = nodeA.startFlow(flow)
        network.runNetwork()

        future.getOrThrow().forEach {
            it.verifySignaturesExcept(nodeA.info.singleIdentity().owningKey)
            it.verifySignaturesExcept(nodeB.info.singleIdentity().owningKey)
        }

    }

    @Test
    fun `recorded Init transaction has only NotarizationState outputs`() {
        val hash = "f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86"
        val flow = NotarizationFlow(hash, nodeB.info.singleIdentity())
        val future = nodeA.startFlow(flow)
        network.runNetwork()
        val initTx = future.getOrThrow()[0]

        // We check the recorded transaction in both vaults.
        for (node in listOf(nodeA, nodeB)) {
            val recordedTx = node.services.validatedTransactions.getTransaction(initTx.id)
            val txOutputs = recordedTx!!.tx.outputs
            assert(txOutputs.size == 1)

            val recordedState = txOutputs[0].data as NotarizationState
            assertEquals(recordedState.hash, hash)
            assertEquals(recordedState.provider, nodeA.info.singleIdentity())
            assertEquals(recordedState.receiver, nodeB.info.singleIdentity())
        }
    }

    @Test
    fun `recorded Notarize transaction has only one input`() {
        val hash = "f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86"
        val flow = NotarizationFlow(hash, nodeB.info.singleIdentity())
        val future = nodeA.startFlow(flow)
        network.runNetwork()
        val notarizeTx = future.getOrThrow()[1]

        // We check the recorded transaction in both vaults.
        for (node in listOf(nodeA, nodeB)) {
            val recordedTx = node.services.validatedTransactions.getTransaction(notarizeTx.id)
            val txInputs = recordedTx!!.tx.inputs
            assert(txInputs.size == 1)
        }
    }

}