package io.rexsio.corda.notarization.state

import net.corda.core.identity.CordaX500Name
import net.corda.testing.core.TestIdentity
import org.junit.Test
import kotlin.test.assertEquals

class NotarizationStateTests {
    private val provider = TestIdentity(CordaX500Name("Provider", "London", "GB"))
    private val receiver = TestIdentity(CordaX500Name("Receiver", "London", "GB"))

    @Test
    fun getStateParticipants() {
        val providerParty = provider.party
        val receiverParty = receiver.party
        val state = NotarizationState("hash", providerParty, receiverParty)

        assertEquals(state.participants, listOf(providerParty, receiverParty))
    }

}