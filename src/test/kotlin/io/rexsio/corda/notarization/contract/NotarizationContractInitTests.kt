package io.rexsio.corda.notarization.contract

import io.rexsio.corda.notarization.contract.NotarizationContract.Companion.ID
import io.rexsio.corda.notarization.state.NotarizationState
import net.corda.core.identity.CordaX500Name
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import org.junit.Test

class NotarizationContractInitTests {
    private val provider = TestIdentity(CordaX500Name("Provider", "London", "GB"))
    private val receiver = TestIdentity(CordaX500Name("Receiver", "London", "GB"))
    private val ledgerServices = MockServices(listOf("net.corda.finance.schemas"), provider, receiver)

    @Test
    fun emptyLedger() {
        ledgerServices.ledger {
        }
    }

    @Test
    fun `verify correct Init command`() {
        val state = getNotarizationState("f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86")

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, receiver.publicKey), NotarizationCommands.Init())
                output(ID, state)
                verifies()
            }
        }
    }

    @Test
    fun `verify should fail when more than one output defined`() {
        val state = getNotarizationState("f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86")

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, receiver.publicKey), NotarizationCommands.Init())
                output(ID, state)
                output(ID, state)
                failsWith("There should be one output state of type NotarizationState.")
            }
        }
    }

    @Test
    fun `verify should fail when input defined`() {
        val state = getNotarizationState("f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86")

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, receiver.publicKey), NotarizationCommands.Init())
                input(ID, state)
                failsWith("There should be no input state of type NotarizationState.")
            }
        }
    }

    @Test
    fun `verify should fail when hash invalid`() {
        val state = getNotarizationState("")

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, receiver.publicKey), NotarizationCommands.Init())
                output(ID, state)
                failsWith("The hash's value must be non-empty.")
            }
        }
    }

    @Test
    fun `verify should fail when signers are the same`() {
        val state = getNotarizationState("f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86")

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, provider.publicKey), NotarizationCommands.Init())
                output(ID, state)
                failsWith("There must be two signers.")
            }
        }
    }

    @Test
    fun `verify should fail when provider and receiver are the same`() {
        val state = NotarizationState(
                "f9e16e8de96b42381d5ce9c7e3c42b858dc06dc396aacfd1a61175b61d0c5b86",
                provider.party,
                provider.party
        )

        ledgerServices.ledger {
            transaction {
                attachments(ID)
                command(listOf(provider.publicKey, receiver.publicKey), NotarizationCommands.Init())
                output(ID, state)
                failsWith("The provider and the receiver cannot be the same entity.")
            }
        }
    }

    private fun getNotarizationState(hash: String): NotarizationState {
        return NotarizationState(hash, provider.party, receiver.party)
    }

}