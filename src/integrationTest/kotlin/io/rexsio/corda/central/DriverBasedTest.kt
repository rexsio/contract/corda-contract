package io.rexsio.corda.central

import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.getOrThrow
import net.corda.testing.core.TestIdentity
import net.corda.testing.driver.DriverDSL
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.NodeHandle
import net.corda.testing.driver.driver
import org.junit.Test
import java.util.concurrent.Future
import kotlin.test.assertEquals

class DriverBasedTest {
    private val notary = TestIdentity(CordaX500Name("Notary", "", "GB"))
    private val rexsioCentral = TestIdentity(CordaX500Name("Rexsio Central", "", "US"))

    @Test
    fun `node test`() = withDriver {
        val (notaryHandle, rexsioCentralBHandle) = startNodes(notary, rexsioCentral)

        assertEquals(rexsioCentral.name, notaryHandle.resolveName(rexsioCentral.name))
        assertEquals(notary.name, rexsioCentralBHandle.resolveName(notary.name))
    }

    private fun withDriver(test: DriverDSL.() -> Unit) = driver(
            DriverParameters(isDebug = true, startNodesInProcess = true)
    ) { test() }

    private fun NodeHandle.resolveName(name: CordaX500Name) = rpc.wellKnownPartyFromX500Name(name)!!.name

    private fun <T> List<Future<T>>.waitForAll(): List<T> = map { it.getOrThrow() }

    private fun DriverDSL.startNodes(vararg identities: TestIdentity) = identities
            .map { startNode(providedName = it.name) }
            .waitForAll()
}